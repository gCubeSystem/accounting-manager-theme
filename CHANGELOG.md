This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "accounting-manager-theme"



## [v1.3.0] - 2021-03-23

### Features

- Migrate accounting-manager to postgresql persistence [#21013]



## [v1.2.0] - 2017-06-12

### Features

- Support Java 8 compatibility [#8460]



## [v1.1.0] - 2016-01-31

### Features

- Updated icon



## [v1.0.0] - 2015-10-15

### Features

- First Release


